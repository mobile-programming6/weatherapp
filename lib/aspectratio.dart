import 'package:flutter/material.dart';

void main() => runApp(WeatherPage());

class WeatherPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        //appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        // body: Container(
        //   color: Colors.cyan[50],
        // ),
      ),
    );
  }

  Widget buildBodyWidget() {
    return ListView(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(32),
          height: 1500,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                  "https://pbs.twimg.com/media/E2IlBD8VIAE1RLy.jpg:large"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(children: <Widget>[
            Text(
              'กรุงเทพมหานคร',
              style: TextStyle(fontSize: 35, color: Colors.white),
            ),
            Text(
              '28°',
              style: TextStyle(fontSize: 100, color: Colors.white),
            ),
            Text(
              'เมฆเป็นส่วนมาก',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.white),
            ),
            Text(
              'สูงสุด: 30° ต่ำสุด: 21°',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.white),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(0),
              height: 150,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.indigo.shade400,
                borderRadius: BorderRadius.circular(20), //border corner radius
                boxShadow: [
                  BoxShadow(
                    color: Colors.white.withOpacity(0), //color of shadow
                    spreadRadius: 5, //spread radius
                    blurRadius: 7, // blur radius
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'คาดว่ามีเมฆบางส่วนประมาณเวลา 18.00',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        height: 2.3,
                      ),
                    ),
                    Divider(
                      indent: 20,
                      thickness: 0,
                      color: Colors.white,
                    ),
                    timeTtems(),
                    iconTtems(),
                  ]),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.all(0),
              height:350,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.indigo.shade400,
                borderRadius: BorderRadius.circular(20), //border corner radius
                boxShadow: [
                  BoxShadow(
                    color: Colors.white.withOpacity(0), //color of shadow
                    spreadRadius: 5, //spread radius
                    blurRadius: 7, // blur radius
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    buildCalendarButton(),
                    Divider(
                      indent: 20,
                      thickness: 0,
                      color: Colors.white,
                    ),
                    buildWeatherToday(),
                    buildWeatherTuesday(),
                    buildWeatherWednesday(),
                    buildWeatherThu(),
                    buildWeatherFri(),
                    buildWeatherSat(),
                    buildWeatherSun(),
                  ]),
            ),
          ]),
        ),
      ],
    );
  }
}

Widget buildWeatherWednesday() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('พ.   ', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('32°', style: TextStyle(color: Colors.white)),
      ]);
}

Widget buildWeatherThu() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('พฤ. ', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('22°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('32°', style: TextStyle(color: Colors.white)),
      ]);
}

Widget buildWeatherFri() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('ศ.   ', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('32°', style: TextStyle(color: Colors.white)),
      ]);
}

Widget buildWeatherSat() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('ส.   ', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('32°', style: TextStyle(color: Colors.white)),
      ]);
}
Widget buildWeatherSun() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('อา. ', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('32°', style: TextStyle(color: Colors.white)),
      ]);
}
Widget buildWeatherTuesday() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('อ.   ', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('20°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('31°', style: TextStyle(color: Colors.white)),
      ]);
}

Widget buildWeatherToday() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('วันนี้', style: TextStyle(color: Colors.white)),
        IconButton(
          icon: Icon(
            Icons.cloud_rounded,
            color: Colors.white,
            size: 20,
            // color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text('21°', style: TextStyle(color: Colors.white)),
        Text('🌡️', style: TextStyle(color: Colors.deepOrange)),
        Text('30°', style: TextStyle(color: Colors.white)),
      ]);
}

Widget timeTtems() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text('13', style: TextStyle(color: Colors.white)),
        Text('14', style: TextStyle(color: Colors.white)),
        Text('15', style: TextStyle(color: Colors.white)),
        Text('16', style: TextStyle(color: Colors.white)),
        Text('17', style: TextStyle(color: Colors.white)),
        Text('18', style: TextStyle(color: Colors.white)),
      ]);
}

Widget buildCalendarButton() {
  return Row(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.calendar_month,
          color: Colors.white,
          size: 20,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text(
        'พยากรณ์อากาศ 7 วัน',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
          height: 0,
          fontWeight: FontWeight.bold,
        ),
      )
    ],
  );
}

Widget buildCloud28Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('28°', style: TextStyle(color: Colors.white)),
    ],
  );
}

Widget buildCloud29Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('29°', style: TextStyle(color: Colors.white)),
    ],
  );
}

Widget buildCloud30Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.amber,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('30°', style: TextStyle(color: Colors.white)),
    ],
  );
}

Widget buildCloud27Button() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.cloud_rounded,
          color: Colors.white,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text('27°', style: TextStyle(color: Colors.white)),
    ],
  );
}

Widget iconTtems() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        buildCloud28Button(),
        buildCloud29Button(),
        buildCloud30Button(),
        buildCloud29Button(),
        buildCloud29Button(),
        buildCloud27Button(),
      ]);
}
